package com.huixian.rabbitmq.model;

/**
 * create at  2018/09/20 14:18
 *
 * @author yanggang
 */
public interface IQueue {

    /**
     * queue名称
     * @return
     */
    String getName();

    /**
     * 无监听者自动删除
     */
    default Boolean exclusive() {
        return Boolean.FALSE;
    }

    /**
     * 获取routingKey
     * @return
     */
    default IRoutingKey getRoutingKey () {
        return null;
    }

    /**
     * 获取ttl时间
     * 默认 60秒
     * @return
     */
    default Integer getTtl () {
        return 60000;
    }
}
