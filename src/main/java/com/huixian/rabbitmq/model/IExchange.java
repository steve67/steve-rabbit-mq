package com.huixian.rabbitmq.model;

/**
 * create at  2018/09/04 16:41
 *
 * @author yanggang
 */
public interface IExchange {

    /**
     * exchange名称
     * @return
     */
    String getName();

}
