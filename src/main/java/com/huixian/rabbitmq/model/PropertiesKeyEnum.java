package com.huixian.rabbitmq.model;

import lombok.AllArgsConstructor;

/**
 * Description:   .
 *
 * @author : yanggang
 * @date : Created in 2018-11-02 0002 14:40
 */
@AllArgsConstructor
public enum PropertiesKeyEnum {

    DXL_RETRY_TIMES("死信队列重试次数"),
    ENVIRONMENT("环境标识"),
    CLUSTER_NAME("集群名称");

    private String desc;

}
