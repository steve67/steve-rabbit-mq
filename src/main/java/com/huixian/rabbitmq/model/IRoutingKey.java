package com.huixian.rabbitmq.model;

/**
 * create at  2018/09/04 16:41
 *
 * @author yanggang
 */
public interface IRoutingKey {

    /**
     * routingKey名称
     * @return
     */
    String getName();

    /**
     * 死信队列重试次数
     * 小于0则无限重试
     * 大于0使用自定义配置
     * 等于0则不重试
     * 其他情况使用默认配置（重试10次）
     * @return
     */
    Integer dxlRetryTimes();


    default IExchange getExchange() {
        return null;
    }
}
