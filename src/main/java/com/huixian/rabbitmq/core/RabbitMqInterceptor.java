package com.huixian.rabbitmq.core;

import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.model.BizException;
import com.huixian.common2.vo.RequestContext;
import com.huixian.rabbitmq.model.PropertiesKeyEnum;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * create at  2018/09/05 11:43
 *
 * @author yanggang
 */
@Component
@Aspect
@Slf4j
public class RabbitMqInterceptor {

    @Value("${dxl.default.retry.times:10}")
    private Integer dxlDefaultRetryTimes;

    /**
     * 定义拦截规则
     */
    @Pointcut("execution (* com.huixian.*.message.rabbit.consumer.*Consumer.*(..))")
    public void annotationPointcut() {
    }

    @Around("annotationPointcut()")
    public Object interceptor(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Method method = getMethod(proceedingJoinPoint);
        if (method.isAnnotationPresent(RabbitHandler.class)) {
            return doInterceptor(proceedingJoinPoint, method);
        } else {
            return proceedingJoinPoint.proceed();
        }
    }

    private Object doInterceptor(ProceedingJoinPoint proceedingJoinPoint, Method method)
            throws Throwable {
        if (proceedingJoinPoint.getArgs().length <= 0) {
            log.error("消息接收方法参数列表为空,class:{},method:{}",
                    proceedingJoinPoint.getTarget().getClass().getSimpleName(), method.getName());
            return null;
        }
        Object param = proceedingJoinPoint.getArgs()[0];
        if (!(param instanceof Message)) {
            log.error("消息接收方法第一个参数必须为Message对象,class:{},method:{}",
                    proceedingJoinPoint.getTarget().getClass().getSimpleName(), method.getName());
            return null;
        }
        Message message = (Message) param;
        try {
            // 处理traceId及token
            init(message);
            log.info("接收到消息,message:{}", message);
            // 验证消息是否需要处理
            if (validMessage(message)) {
                return proceedingJoinPoint.proceed();
            }

        } catch (Exception e) {
            if (validMessage(message)) {
                throwMessageInDxl(e);
            }
        } finally {
            log.debug("整个请求处理完毕清除 logback MDC");
            MDC.clear();
            RequestContext.clear();
        }
        return null;
    }

    /**
     * 把消息抛入死信队列
     */
    private void throwMessageInDxl(Exception e) {
        if (e instanceof BizException) {
            BizException bizException = (BizException) e;
            log.info(StringUtils.join("errorDesc", bizException.getMessage()));
        } else {
            log.error(e.getMessage());
            throw new AmqpRejectAndDontRequeueException("消息消费异常进入死信队列", e);
        }
    }


    /**
     * 验证消息最多循环补偿10次
     */
    private boolean validMessage(Message message) {
        boolean flag;
        ArrayList<Map<String, Object>> xDeathMap = (ArrayList<Map<String, Object>>) message
                .getMessageProperties().getHeaders().get("x-death");
        Long count = null;
        String exChange = null;
        String queue = null;
        if (xDeathMap != null && xDeathMap.size() > 1 && xDeathMap.get(1).get("count") != null) {
            count = Long.valueOf(xDeathMap.get(1).get("count").toString());
            exChange = xDeathMap.get(1).get("exchange").toString();
            queue = xDeathMap.get(1).get("queue").toString();
        }
        // 延迟队列特殊处理
        else if (xDeathMap != null && xDeathMap.size() == 1) {
            return true;
        }

        Integer times = dxlDefaultRetryTimes;
        // 从header头中获取发送时指定的重试次数
        Object dxlRetryTimes = message.getMessageProperties().getHeaders()
                .get(PropertiesKeyEnum.DXL_RETRY_TIMES.name());
        // 不为null则尝试处理，为null走默认逻辑
        if (dxlRetryTimes != null && count != null) {
            try {
                Integer retryTimes = (Integer) dxlRetryTimes;
                // 队列发送时设置重试次数为-1，则无限循环
                if (retryTimes < 0) {
                    log.info(StringUtils
                            .join("exchange:", exChange, ",queue:", queue, ",correlationId:",
                                    message.getMessageProperties().getCorrelationId(),
                                    ",循环消费,消费次数", count, "次"));
                    return true;
                } else if (retryTimes > 0) {
                    times = retryTimes;
                }
                // 0则不重试
                else {
                    return false;
                }
            } catch (Exception e) {
                // 异常情况走默认逻辑
            }

        }

        if (count != null && count > times) {
            log.info(StringUtils.join("exchange:", exChange, ",queue:", queue, ",correlationId:",
                    message.getMessageProperties().getCorrelationId(),
                    ",消费次数超过", times, "次取消消费"));
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }


    private void init(Message message) {
        Map<String, Object> headers = message.getMessageProperties().getHeaders();
        if (headers != null) {
            for (String key : headers.keySet()) {
                RequestContextEnum requestContextEnum = RequestContextEnum.getByOutCode(key);
                if (requestContextEnum != null) {
                    RequestContext.getContext().addContextDetail(requestContextEnum.name(),
                            headers.get(key) == null ? null : headers.get(key).toString());
                } else {
                    RequestContext.getContext().addContextDetail(key,
                            headers.get(key) == null ? null : headers.get(key).toString());
                }
            }
        }

        if (RequestContext.getContext().getContextDetail(RequestContextEnum.TRACE_ID.name())
                == null) {
            RequestContext.getContext().addContextDetail(RequestContextEnum.TRACE_ID.name(),
                    UUID.randomUUID().toString());
        }
        MDC.put(RequestContextEnum.TRACE_ID.name(),
                RequestContext.getContext().getContextDetail(RequestContextEnum.TRACE_ID.name()));
    }

    private Method getMethod(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        return pjp.getTarget().getClass()
                .getMethod(pjp.getSignature().getName(), methodSignature.getParameterTypes());
    }

}
