package com.huixian.rabbitmq.core;

import static com.huixian.rabbitmq.model.PropertiesKeyEnum.CLUSTER_NAME;
import static com.huixian.rabbitmq.model.PropertiesKeyEnum.DXL_RETRY_TIMES;
import static com.huixian.rabbitmq.model.PropertiesKeyEnum.ENVIRONMENT;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.model.CommonError;
import com.huixian.common2.model.SystemException;
import com.huixian.common2.util.EnvironmentUtil;
import com.huixian.common2.util.TokenUtil;
import com.huixian.common2.vo.RequestContext;
import com.huixian.rabbitmq.model.IExchange;
import com.huixian.rabbitmq.model.IRoutingKey;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * create at  2018/09/04 16:35
 *
 * @author yanggang
 */
@Service
@Slf4j
public class RabbitMqSendManage implements RabbitTemplate.ConfirmCallback {

    public static final String CLUSTER_PROPERTY_NAME = "cluster.name";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private GsonBuilder gsonBuilder;

    /**
     * 发送消息
     *
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写发送
     * @param data 待发送数据
     */
    public void send(IRoutingKey routingKey, Object data) {
        if(routingKey.getExchange() == null) {
            throw new IllegalArgumentException("routingKey对应的exchange为空");
        }
        send(routingKey.getExchange(), routingKey, data, null,null);
    }

    /**
     * 发送消息
     *
     * @param exchange exchange枚举，使用枚举的getName方法转小写发送
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写发送
     * @param data 待发送数据
     */
    public void send(IExchange exchange, IRoutingKey routingKey, Object data) {
        send(exchange, routingKey, data, null,null);
    }

    /**
     * 发送消息
     *
     * @param exchange exchange枚举，使用枚举的getName方法转小写发送
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写发送
     * @param data 待发送数据
     * @param correlationId 消息标识，可不传，默认当前时间戳
     */
    public void send(IExchange exchange, IRoutingKey routingKey, Object data,String correlationId) {
        send(exchange, routingKey, data, null,correlationId);
    }

    /**
     * 发送消息
     *
     * @param exchange exchange枚举，使用枚举的getName方法转小写发送
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写发送
     * @param data 待发送数据
     * @param delayMillis 延迟毫秒数
     */
    public void send(IExchange exchange, IRoutingKey routingKey, Object data,
            Long delayMillis) {
        send(exchange, routingKey, data, delayMillis, null);
    }

    /**
     * 发送消息
     *
     * @param exchange exchange枚举，使用枚举的getName方法转小写发送
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写发送
     * @param data 待发送数据
     * @param delayMillis 延迟毫秒数
     * @param correlationId 消息标识，可不传，默认当前时间戳
     */
    public void send(IExchange exchange, IRoutingKey routingKey, Object data, Long delayMillis,
            String correlationId) {
        send(exchange, routingKey, null, data, delayMillis, correlationId);
    }

    /**
     * 发送消息
     *
     * @param exchange exchange枚举，使用枚举的getName方法转小写发送
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写发送
     * @param messageProperties messageProperties对象，可不传，默认将token及traceId加到消息header中
     * @param data 待发送数据
     * @param delayMillis 延迟毫秒数
     * @param correlationId 消息标识，可不传，默认当前时间戳
     */
    public void send(IExchange exchange, IRoutingKey routingKey,
            MessageProperties messageProperties, Object data, Long delayMillis,
            String correlationId) {
        if (exchange == null || routingKey == null) {
            log.error("消息发送失败，exchange或routingKey不可为空");
            return;
        }
        if (exchange.getName() == null || routingKey.getName() == null) {
            log.error("消息发送失败，exchange的name或routingKey的name不可为空");
            return;
        }
        if (data == null) {
            log.error("消息发送失败，data不可为空");
            return;
        }
        // 发送消息
        convertAndSend(exchange.getName().toLowerCase(), routingKey.getName().toLowerCase(),
                routingKey.dxlRetryTimes(), messageProperties, data, delayMillis, correlationId);
    }

    /**
     * 调用RabbitMQ服务发送消息
     *
     * @param messageProperties messageProperties对象，可不传，默认将token及traceId加到消息header中
     * @param data 发送消息数据内容
     * @param correlationId 消息标识，可不传，默认当前时间戳
     */
    private void convertAndSend(String exchange, String routingKey,
            Integer dxlRetryTimes, MessageProperties messageProperties, Object data,
            Long delayMillis, String correlationId) {
        try {
            CorrelationData correlationData = new CorrelationData();
            if (StringUtils.isBlank(correlationId)) {
                correlationData.setId(String.valueOf(System.currentTimeMillis()));
            } else {
                correlationData.setId(correlationId);
            }

            rabbitTemplate.setConfirmCallback(this);
            // 此处必须使用gsonBuilder创建Gson对象，否则LocalDateTime、LocalDate、LocalTime序列化有问题
            Gson gson = gsonBuilder.create();
            String msgData = gson.toJson(data);
            // 将traceId和token放入header中
            if (messageProperties == null) {
                messageProperties = new MessageProperties();
            }
            messageProperties.setHeader(RequestContextEnum.TRACE_ID.getOutCode(),
                    RequestContext.getContext()
                            .getContextDetail(RequestContextEnum.TRACE_ID.name()));
            messageProperties.setHeader(RequestContextEnum.HX_TOKEN.getOutCode(),
                    TokenUtil.getActiveToken());
            messageProperties.setHeader(DXL_RETRY_TIMES.name(), dxlRetryTimes);
            if (delayMillis != null && delayMillis > 0) {
                messageProperties.setExpiration(delayMillis.toString());
            }

            String clusterName = System.getProperty(CLUSTER_PROPERTY_NAME);
            if(StringUtils.isNotBlank(clusterName)){
                messageProperties.setHeader(CLUSTER_NAME.name(), clusterName);
            }
            Message message = rabbitTemplate.getMessageConverter()
                    .toMessage(msgData, messageProperties);
            // 默认发送至小写exchange及routingKey中
            rabbitTemplate.convertAndSend(exchange,
                    routingKey, message, correlationData);
            log.info(StringUtils.join(
                    "消息发送成功，exchange:", exchange
                    , ",routingKey:", routingKey
                    , ",correlationId:", correlationId, ",data:", data));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(StringUtils.join(
                    "消息发送失败，exchange:", exchange
                    , ",routingKey:", routingKey
                    , ",correlationId:", correlationId, ",data:", data
                    , ",原因:", e.getMessage()));
            throw new SystemException(CommonError.AMQP_SEND_ERROR);
        }
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            log.info("消息回调成功，回调id:" + correlationData);
        } else {
            log.error("消息回调失败，回调id: {}", correlationData);
        }
    }
}
