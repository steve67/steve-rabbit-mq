package com.huixian.rabbitmq.util;

import com.alibaba.fastjson.JSONObject;
import java.util.Map;
import java.util.Set;
import org.springframework.amqp.core.Message;

/**
 * Description:   .
 *
 * @author : yanggang
 * @date : Created in 2018-10-31 0031 9:27
 */
public class RabbitMqMessageParseUtil {

    private static final String OLD_FIX_KEY = "messageObject";

    /**
     * 解析message
     * @param message
     * @param t 目标类字节码
     * @param <T> 目标类
     * @return 目标类实例
     */
    public static <T> T parseMessage(Message message, Class<T> t) {
        return parseMessage(message, t, null);
    }

    /**
     * 解析message
     * @param message
     * @param t 目标类字节码
     * @param fixedKeyMap 兼容key，map中key为消息体中的key，value为本地需要的key
     * @param <T> 目标类
     * @return 目标类实例
     */
    public static <T> T parseMessage(Message message, Class<T> t, Map<String, String> fixedKeyMap) {
        if (message == null || t == null) {
            return null;
        }
        String body = new String(message.getBody());
        JSONObject data = JSONObject.parseObject(body);
        if (data.containsKey(OLD_FIX_KEY)) {
            data = data.getJSONObject(OLD_FIX_KEY);
        }
        if (fixedKeyMap != null && !fixedKeyMap.isEmpty()) {
            Set<String> keySet = fixedKeyMap.keySet();
            for (String key : keySet) {
                if (data.containsKey(key)) {
                    data.put(fixedKeyMap.get(key), data.get(key));
                }
            }
        }
        return JSONObject.parseObject(data.toJSONString(), t);
    }

}
