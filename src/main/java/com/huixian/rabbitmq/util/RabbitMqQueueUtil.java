package com.huixian.rabbitmq.util;

import com.google.common.collect.ImmutableMap;
import com.huixian.common2.model.CommonError;
import com.huixian.common2.model.SystemException;
import com.huixian.rabbitmq.model.IExchange;
import com.huixian.rabbitmq.model.IQueue;
import com.huixian.rabbitmq.model.IRoutingKey;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.GenericApplicationContext;

/**
 * create at  2018/09/05 16:20
 *
 * @author yanggang
 */
@Slf4j
public class RabbitMqQueueUtil {

    /**
     * 构建队列
     * 默认ttl 10秒
     */
    public static void buildQueue(IQueue iQueue, GenericApplicationContext applicationContext) {
        buildQueue(iQueue, iQueue.getTtl(), applicationContext);
    }

    /**
     * 构建队列
     */
    public static void buildQueue(IQueue iQueue, long ttl, GenericApplicationContext applicationContext) {
        if(iQueue.getRoutingKey() == null) {
            throw new IllegalArgumentException("队列对应的routingKey为空");
        }
        if(iQueue.getRoutingKey().getExchange() == null) {
            throw new IllegalArgumentException("队列对应的exchange为空");
        }
        buildQueue(iQueue.getRoutingKey().getExchange(), iQueue.getRoutingKey(), iQueue, ttl, applicationContext);
    }

    /**
     * 构建队列
     *
     * @param exchange exchange枚举，使用枚举的getName方法转小写构建
     * @param routingKey routingKey枚举，使用枚举的getName方法转小写构建
     * @param iQueue queue枚举，使用枚举的getName方法转小写构建，可不传，默认使用和routingKey的name构建queue
     * @param ttl 超时时间  默认1秒
     * @param applicationContext 必传
     */
    public static void buildQueue(IExchange exchange, IRoutingKey routingKey, IQueue iQueue,
            long ttl, GenericApplicationContext applicationContext) {
        if (applicationContext == null) {
            log.error("applicationContext不可为空");
            throw new SystemException(CommonError.PARAM_ERROR);
        }
        if (exchange == null || routingKey == null || StringUtils.isBlank(exchange.getName())
                || StringUtils.isBlank(routingKey.getName())) {
            log.error("exchange或routingKey不可为空");
            throw new SystemException(CommonError.PARAM_ERROR);
        }
        if (iQueue != null && StringUtils.isBlank(iQueue.getName())) {
            log.error("queue的name不可为空");
            throw new SystemException(CommonError.PARAM_ERROR);
        }

        boolean exclusive = false;
        if (iQueue != null && iQueue.exclusive() != null) {
            exclusive = iQueue.exclusive();
        }

        String exchangeName = exchange.getName().toLowerCase();
        String routingKeyName = routingKey.getName().toLowerCase();
        String queueName;
        if (iQueue == null) {
            queueName = routingKeyName;
        } else {
            queueName = iQueue.getName().toLowerCase();
        }
        //构建队列
        buildQueue(exchangeName, routingKeyName, queueName, exclusive, ttl, applicationContext);
    }

    /**
     * 构建队列
     *
     * @param exchangeName exchange
     * @param routingKeyName routingKey
     * @param queueName 可不传，默认使用和routingKey的name构建queue
     * @param ttl 超时时间  默认1秒
     * @param applicationContext 必传
     */
    private static void buildQueue(String exchangeName, String routingKeyName, String queueName,
            Boolean exclusive,
            long ttl, GenericApplicationContext applicationContext) {
        if (applicationContext == null) {
            log.error("applicationContext不可为空");
            throw new SystemException(CommonError.PARAM_ERROR);
        }
        if (StringUtils.isBlank(exchangeName)) {
            log.error("exchangeName不可为空");
            throw new SystemException(CommonError.PARAM_ERROR);
        }
        if (StringUtils.isBlank(routingKeyName)) {
            log.error("routingKeyName不可为空");
            throw new SystemException(CommonError.PARAM_ERROR);
        }
        if (StringUtils.isBlank(queueName)) {
            queueName = routingKeyName;
        }
        // ttl不写则默认为1秒
        if (ttl <= 0) {
            ttl = 1000L;
        }

        //构建死信队列
        String dxlQueueName = buildDxlQueueName(queueName, ttl);

        // 使用topic类型
        TopicExchange topicExchange = new TopicExchange(exchangeName, true, false);
        // 普通队列
        final ImmutableMap<String, Object> args = ImmutableMap.of("x-dead-letter-exchange", "",
                "x-dead-letter-routing-key",
                dxlQueueName);
        Queue queue = new Queue(queueName, true, exclusive, false, args);
        // 死信队列
        final ImmutableMap<String, Object> args2 = ImmutableMap.of("x-dead-letter-exchange", "",
                "x-dead-letter-routing-key", queueName, "x-message-ttl", ttl);
        Queue dxlQueue = new Queue(dxlQueueName, true, exclusive, false, args2);

        // 构建绑定关系
        Binding binding = BindingBuilder.bind(queue).to(topicExchange).with(routingKeyName);

        // 注册相应的单例bean至spring容器中
        BeanDefinitionBuilder topicExchangeBuilder = BeanDefinitionBuilder
                .genericBeanDefinition(TopicExchange.class);
        BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
                .genericBeanDefinition(Queue.class);
        BeanDefinitionBuilder bindingBuilder = BeanDefinitionBuilder
                .genericBeanDefinition(Binding.class);

        applicationContext.registerBeanDefinition(StringUtils.join(exchangeName, "Exchange"),
                topicExchangeBuilder.getBeanDefinition());
        applicationContext.getBeanFactory()
                .registerSingleton(StringUtils.join(exchangeName, "Exchange"), topicExchange);

        applicationContext.registerBeanDefinition(StringUtils.join(queueName, "Queue"),
                queueBuilder.getBeanDefinition());
        applicationContext.getBeanFactory()
                .registerSingleton(StringUtils.join(queueName, "Queue"), queue);

        applicationContext.registerBeanDefinition(StringUtils.join(queueName, "DxlQueue"),
                queueBuilder.getBeanDefinition());
        applicationContext.getBeanFactory()
                .registerSingleton(StringUtils.join(queueName, "DxlQueue"), dxlQueue);

        applicationContext
                .registerBeanDefinition(StringUtils.join(exchangeName, routingKeyName, "Bind"),
                        bindingBuilder.getBeanDefinition());
        applicationContext.getBeanFactory()
                .registerSingleton(StringUtils.join(exchangeName, routingKeyName, "Bind"), binding);

    }

    /**
     * 构建死信队列名称
     */
    private static String buildDxlQueueName(String tradeQueueName, Long ttl) {
        return StringUtils.join(tradeQueueName, "__dlx_requeue_", ttl, "ms");
    }

}
