package com.huixian.rabbitmq;

import com.huixian.rabbitmq.core.RabbitMqConfig;
import com.huixian.rabbitmq.core.RabbitMqInterceptor;
import com.huixian.rabbitmq.core.RabbitMqSendManage;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * create at  2018/08/22 10:53
 *
 * @author yanggang
 */
@Configuration
@Import(value = {RabbitMqSendManage.class
        , RabbitMqConfig.class
        , RabbitMqInterceptor.class})
public class RabbitAutoConfiguration {

}
