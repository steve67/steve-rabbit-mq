#!/bin/bash

#测试token = fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171
#正式token = fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171

_DINGTALK_TOKEN=fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

_ip_addr=$(hostname),$(ip route | grep -oP '(?<=src )[0-9.]+' | tail -1 )

_TITLE="[编译失败]"

_TEXT="** ${GITLAB_USER_NAME} ** 提交的 [${CI_COMMIT_SHA:0:7}](${CI_PROJECT_URL}/commit/${CI_COMMIT_SHA}) * 编译失败 *!  \n
> 分支: [${CI_COMMIT_REF_NAME} @ ${CI_PROJECT_PATH}](${CI_PROJECT_URL}/commits/${CI_COMMIT_REF_NAME})  \n
> 环境: ${CI_RUNNER_DESCRIPTION} @ ${_ip_addr} \n
> 目录: ${CI_PROJECT_DIR}  \n
> 错误: [最后10行](${CI_PROJECT_URL}/pipelines/${CI_PIPELINE_ID}/failures)  \n
  \n
  \n
  \n
[查看job详情](${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID})"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 123
